import * as firebase from 'firebase';

export class Comment{

  date: number;
  author: string;
  comment: string;

  constructor(comment: string) {
    // TODO revoir la date
    this.date = new Date().getTime();
    this.author = firebase.auth().currentUser.email;
    this.comment = comment;
  }
}
