import {Comment} from './commentaire.model';

export class Serie{

  name: string;
  dateBegin: number;
  nbrSeasons: number;
  description: string;
  critic: string;
  photo: string;
  comment: Array<Comment>;

  constructor(name: string, dateBegin: number, nbrSeasons: number, description: string, critic: string, photo: string) {
    this.name = name;
    this.dateBegin = dateBegin;
    this.nbrSeasons = nbrSeasons;
    this.description = description;
    this.critic = critic;
    this.photo = photo;
  }
}
