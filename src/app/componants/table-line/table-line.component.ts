import {Component, Input, OnInit} from '@angular/core';
import {SerieService} from '../../services/serie/serie.service';

@Component({
  selector: '[app-table-line]',
  templateUrl: './table-line.component.html',
  styleUrls: ['./table-line.component.css']
})
export class TableLineComponent implements OnInit{

  @Input() serieId;
  @Input() serieName;
  @Input() serieDateDebut;

  detailsURL: any;
  editURL: any;

  constructor(private serieService: SerieService) { }

  ngOnInit(): void {
    this.detailsURL = '/serie/' + this.serieId;
    this.editURL = '/serie/edit/' + this.serieId;
  }

  onClickDeleteSerie(): void {
    this.serieService.removeSerie(this.serieId);
  }
}
