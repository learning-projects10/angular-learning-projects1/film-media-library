import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import * as firebase from 'firebase';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isAuth: boolean;

  chemin = this.route.snapshot.routeConfig.path;

  constructor(private authService: AuthService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    firebase.auth().onAuthStateChanged(
      (user) => {
        if (user) {
          this.isAuth = true;
        } else {
          this.isAuth = false;
        }
      });
  }

  onClickSignOut(): void {
    this.authService.signOutUser();
  }
}
