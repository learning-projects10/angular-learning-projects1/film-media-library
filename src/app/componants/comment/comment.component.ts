import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  @Input() commentDate;
  @Input() commentAuthor;
  @Input() commentComment;

  constructor() { }

  ngOnInit(): void {
  }

}
