import {Component, OnDestroy, OnInit} from '@angular/core';
import {Serie} from '../../models/serie.model';
import {Subscription} from 'rxjs';
import {SerieService} from '../../services/serie/serie.service';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit, OnDestroy {

  series: Array<Serie>;
  seriesSubscription: Subscription;

  constructor(private serieService: SerieService) {
  }

  ngOnInit(): void {
    this.seriesSubscription = this.serieService.seriesSubject.subscribe(
      (series: Array<Serie>) => {
        this.series = series;
      });
    this.serieService.emitSeriesSubject();
  }

  ngOnDestroy(): void {
    this.seriesSubscription.unsubscribe();
  }

}
